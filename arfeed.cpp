/*
 * Soubor:    arfeed.cpp
 * Vytvořeno: 3. říjen 2015, 19:48
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 */

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>

#include "input.h"
#include "network.h"
#include "feed.h"

using namespace std;

bool getFormattedFeedInfo(InputProgramParams inProgParams, vector<string> listOfFeeds, string* formattedFeedInfo);

/**
 * Vstupní bod programu. Koordinuje provádění akcí, které vedou k získání formátovaného řetězce reprezentující
 * vybrané informace ze zdrojů novinek. Následně tento řetězec vypíše na stdout.
 * @param argc počet argumentů předaných programu
 * @param argv pole argumentů předaných programu
 * @return EXIT_FAILURE - v případě jakékoliv chyby
 *         EXIT_SUCCESS - v případě že nenastala žádná chyba
 */
int main(int argc, char** argv)
{
    // struktura obsahující vstupní parametry programu
    InputProgramParams inProgParams;

    // zpracujeme a načteme parametry programu, v případě chyby ukončíme program
    if (processTheParams(argc, argv, &inProgParams) == false) {
        exit(EXIT_FAILURE);
    }

    // pokud chtěl uživatel vytisknou nápovědu, ukončíme program
    if (inProgParams.printHelpFlag == true) {
        exit(EXIT_SUCCESS);
    }

    // seznam obsahující zdroje ve formátu URL
    vector<string> listOfFeeds;

    // naplníme seznam zdrojů, v případě chyby ukončíme program
    if (fillListOfFeeds(inProgParams, &listOfFeeds) == false) {
        exit(EXIT_FAILURE);
    }

    // řetězec obsahující formátované informace o zdrojích, připravené k vytištění na stdout
    string formattedFeedInfo = "";

    // zavoláme funkci, která nám na základě seznamu zdrojů vrátí řetězec požadovaných formátovaných informací o zdrojích
    // pokud dojde k jakékoliv chybě, vytiskneme informace o zdrojích a ukončíme program s chybovým návratovým kódem
    if (getFormattedFeedInfo(inProgParams, listOfFeeds, &formattedFeedInfo) == false) {
        cout << formattedFeedInfo;
        exit(EXIT_FAILURE);
    }
    // vytiskneme řetězec s informacemi o zdrojích a ukončíme program
    cout << formattedFeedInfo;
    exit(EXIT_SUCCESS);

}

/**
 * Funkce na základě seznamu URL zdrojů vrátí řetězec obsahující požadované formátované informace o daných zdrojích
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @param listOfFeeds seznam obsahující zdroje ve formátu URL
 * @param formattedFeedInfo výstupní parametr, přes který funkce předá formátovaný řetězec informací o zdrojích
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool getFormattedFeedInfo(InputProgramParams inProgParams, vector<string> listOfFeeds, string* formattedFeedInfo) {

    // příznak indikující že došlo k chybě při stahování nebo parsování zdrojů
    // a program by se měl tedy ukončit s chybným návratovým kódem
    bool exitWithError = false;

    Url url;                             // Struktura reprezentující URL
    string feedContent;                  // obsah zdroje vrácený serverem
    string outputFormattedStringForFeed; // řetězec obsahující naformátované informace pro daný zdroj

    int numberOfSuccessTreatetFeeds = 0; // počet úspěšně zpracovaných zdrojů
    // postupně pro každý zdroj ze seznamu zdrojů získáme požadované informace
    for (size_t indexOfFeed = 0; indexOfFeed < listOfFeeds.size(); indexOfFeed++) {

        // inicializace proměnných
        feedContent = "";
        outputFormattedStringForFeed = "";
        url.url = listOfFeeds.at(indexOfFeed);

        // získáme strukturu s jednotlivými částmi URL
        if (parseUrl(&url) == false) {
            exitWithError = true;
            continue;
        }

        // získáme obsah zdroje na dané URL adrese
        if (getContentFromServer(url, inProgParams, &feedContent, 1) == false) {
            exitWithError = true;
            continue;
        }

        // zpracujeme obsah zdroje a získáme řetězec s formátovanými informacemi o daném zdroji
        if (!parseFeed(feedContent, url, &outputFormattedStringForFeed, inProgParams)) {
            exitWithError = true;
            continue;
        }

        // oddělíme v celkovém výpise zdroje od sebe
        if (numberOfSuccessTreatetFeeds > 0) {
            *formattedFeedInfo += "\n";
        }
        // řetězec s formátovanými informacemi o daném zdroji připojíme k celkovému řetězci s informacemi o zdrojích
        *formattedFeedInfo += outputFormattedStringForFeed;
        numberOfSuccessTreatetFeeds++;
    }

    // pokud došlo alespoň k jedné chybě vracíme false
    if (exitWithError) {
        return false;
    }
    else {
        return true;
    }
}
