/*
 * Soubor:    feed.h
 * Vytvořeno: 7. listopad 2015, 20:06
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce pro práci se staženými zdroji ve formátu Atom/RSS (zejména jejich parsování a získání formátovaných informací)
 */

#ifndef FEED_H_INCLUDED
#define FEED_H_INCLUDED

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <string>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "network.h"
#include "input.h"

using namespace std;

/**
 * Funkce zpracuje obsah daného zdroje novinek a vrátí řetězec obsahující formátované požadované informace z daného zdroje.
 * Pro zpracování jednotlivých formátů zdrojů novinek volá dedikované funkce.
 * @param xmlContent obsah zdroje novinek ve formátu XML
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param outputFormattedStringForFeed výstupní parametr, přes který funkce vrací formátovaný řetězec,
                                       obsahující formátované požadované informace z daného zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseFeed (string xmlContent, Url url, string *outputFormattedStringForFeed, InputProgramParams inProgParams);

#endif // FEED_H_INCLUDED
