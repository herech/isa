/*
 * Soubor:    network.h
 * Vytvořeno: 31. říjen 2015, 22:13
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce a ADT pro práci v síťovém prostředí (parsování url, vytváření zabezpečeného a nezabezpečeného spojení, stahování dat, ...)
 */

#ifndef NETWORK_H_INCLUDED
#define NETWORK_H_INCLUDED

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <string>

#include "openssl/bio.h"
#include "openssl/ssl.h"

#include "input.h"

using namespace std;

// struktura uchovávající celé url, ale i jeho části
typedef struct {
    string url;
    string protocol;
    string portNumber;
    string hostName;
    string localResource;
} Url;

/**
 * Funkce má jako vstupní i výstupní parametr zároveň strukturu Url (deklarovaná v network.h).
 * Před voláním funkce je potřeba nastavit složku url struktury Url a funkce sama nastaví ostatní složky této struktury
 * @param url vstupně/výstupní parametr, kterým je struktura obsahující URL (včetně jeho podčástí) (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseUrl(Url* url);

/**
 * Funkce vrátí obsah zdroje na dané URL adrese (tělo HTTP(S) odpovědi serveru)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @param content výstupní parametr, přes který funkce vrací řetězec, který reprezentuje obsah zdroje
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool getContentFromServer(Url url, InputProgramParams inProgParams, string* content, int deep);


#endif // NETWORK_H_INCLUDED
