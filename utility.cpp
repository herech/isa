/*
 * Soubor:    utility.cpp
 * Vytvořeno: 12. listopad 2015, 23:55
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje různé obecné užitečné funkce s širokým potenciálem použití
 */

#include <string>

#include "utility.h"

using namespace std;

/**
 * Funkce odstraní okrajové bílé znaky z daného řetězce
 * @param text řetězec, který potenciálně obsahuje okrajový bíle znaky
 * @return řetězec bez okrajových bílých znaků
 */
string trim (string text) {
    size_t leftTextPos = text.find_first_not_of(" \n\r\t\v\f");
    size_t rightTextPos = text.find_last_not_of(" \n\r\t\v\f");
    return text.substr(leftTextPos, rightTextPos - leftTextPos + 1);
}

/**
 * Funkce zjistí, jestli je řetězec nezáporné číslo
 * @param str řetezec, který ověřujeme
 * @return false - v případě řetězec není číslo
 *         true  - v případě řetězec je číslo
 */
bool isNonNegativeNumber(string str) {
    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    return str.find_first_not_of("0123456789") == string::npos;

    // <PŘEVZATÝ KÓD | KONEC>
}
