/*
 * Soubor:    input.cpp
 * Vytvořeno: 31. říjen 2015, 18:16
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce a ADT pro práci se vstupem (načtení a zpracování vstupních parametrů, načtení zdrojů ze souboru, ...)
 */

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <errno.h>

#include "input.h"

using namespace std;

// nápověda programu
const string help =
    "\n\t --- PROGRAM ARFEED --- \n\n"

    "Popis:\n"
        "\tProgram arfeed stahuje zadané zdroje (ve formátu Atom/RSS) na základě jejich URL a vypisuje z nich požadované informace na stdout\n\n"

    "Použití\n"
        "\t./arfeed {urlOfFeed | -f feedFile} [-c certFile] [-C certDir] [-l] [-T] [-a] [-u] [-h]\n\n"

    "Parametry:\n"
        "\t{urlOfFeed | -f feedFile}"
            " - Povinně je uveden buď urlOfFeed (URL požadovaného zdroje), nebo parametr -f s hodnotou feedFile (určuje umístění souboru feedFile). "
            "Soubor feedFile je textový soubor, kde je na každém řádku uvedena jedna adresa zdroje ve formátu Atom/RSS. "
            "Prázdné řádky a řádky začínající znakem '#' jsou ignorovány.\n\n"

        "\t[-c certFile]"
            " - Volitelný parametr -c s hodnotou certFile - určující soubor s certifikáty, který se použije pro ověření platnosti certifikátu SSL/TLS serveru.\n\n"

        "\t[-C certDir]"
            " - Volitelný parametr -C s hodnotou certDir - určující adresář ve kterém se mají vyhledávat certifikáty, které se použijí pro ověření platnosti certifikátu SSL/TLS serveru. "
            "Výchozí hodnota je /etc/ssl/certs.\n\n"

        "\t[-l]"
            " - Volitelný parametr -l znamená, že pro každý zdroj se budou vypisovat pouze informace o nejnovějším záznamu.\n\n"

        "\t[-T]"
            " - Volitelný parametr -T znamená, že se pro každý záznam zobrazí navíc informace o čase změny záznamu (je-li ve staženém souboru obsaženo).\n\n"

        "\t[-a]"
            " - Volitelný parametr -a znamená, že se pro každý záznam zobrazí jméno autora (je-li ve staženém souboru obsaženo).\n\n"

        "\t[-u]"
            " - Volitelný parametr -u znamená, že se pro každý záznam zobrazí asociované URL (je-li ve staženém souboru obsaženo).\n\n"

        "\t[-h]"
            " - Volitelný parametr -h znamená, že se vypíše nápověda a program se ukončí\n\n";

/**
 * Funkce která zpracuje vstupní parametry a tyto vrací ve struktuře
 * @param argc počet argumentů předaných programu
 * @param argv pole argumentů předaných programu
 * @param inProgParams výstupní parametr, jedná se o strukturu obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool processTheParams(int argc, char** argv, InputProgramParams *inProgParams) {

    // inicializujeme strukturu obsahující vstupní parametry
    inProgParams->certFileDir = "/etc/ssl/certs";
    inProgParams->feedUrlFlag = false;
    inProgParams->feedFileFlag = false;
    inProgParams->certFileFlag = false;
    inProgParams->printHelpFlag = false;
    inProgParams->showNewestRecordFlag = false;
    inProgParams->showUpdatedFlag = false;
    inProgParams->showAuthorFlag = false;
    inProgParams->showRelatedUrlFlag = false;

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Copyright 2015 Free Software Foundation, Inc.
    // Titulek: Example of Parsing Arguments with getopt
    // Dostupné z: http://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html#Example-of-Getopt
    // Doslovné kopírování a šíření tohoto kódu je dovoleno komukoliv za předpokladu, tato poznámka bude zachována.
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    int ch;     // proměnná, do které getopt ukládá název aktuálně načteného parametru
    opterr = 0; // vypneme výchozí výpis chyb spojených s argumenty

    // pomocí getopt zpracujeme argumenty programu
    while ((ch = getopt(argc, argv, "f:c:C:lTauh")) != EOF) {

        switch (ch) {

            // parametr -f pro soubor obsahující url zdrojů ve formátu Atom
            case 'f':
                inProgParams->feedFileFlag = true;
                inProgParams->feedFile = string(optarg);
                break;

            // parametr -c pro soubor obsahující certifikáty
            case 'c':
                inProgParams->certFileFlag = true;
                inProgParams->certFile = string(optarg);
                break;

            // parametr -C pro složku obsahující soubory s certifikáty
            case 'C':
                inProgParams->certFileDir = string(optarg);
                break;

            // parametr -l jako příznak, který určuje, jestli se má ke každému zdroji vypisovat pouze info k jeho nejnovějšímu záznamu
            case 'l':
                inProgParams->showNewestRecordFlag = true;
                break;

            // parametr -T jako příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o čase změny (pokud je dostupná)
            case 'T':
                inProgParams->showUpdatedFlag = true;
                break;

            // parametr -a jako příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o jeho autorovi (pokud je dostupná)
            case 'a':
                inProgParams->showAuthorFlag = true;
                break;

            // parametr -u jako příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o asociovaném URL (pokud je dostupná)
            case 'u':
                inProgParams->showRelatedUrlFlag = true;
                break;

            // parametr -h jako příznak, který určuje, jestli se má vypsat nápověda
            case 'h':
                inProgParams->printHelpFlag = true;
                cout << help << endl;
                return true;

            // pokud dojde k chybě (typicky neznámý parametr, chybějící hodnota parametru)
            default:
                fprintf(stderr, "\nChyba: Neznámý parametr nebo chybějící hodnota parametru!\n");
                fprintf(stderr, "Nápověda: zadejte parametr -h pro výpis nápovědy!\n");
                return false;
        }
    }

    // pokud byl zadán argument, který není specifikován přepínačem, jedná se o url zdroje
    if (argc != optind) {
       inProgParams->urlOfFeed = string(argv[optind]);
       inProgParams->feedUrlFlag = true;
    }

    // <PŘEVZATÝ KÓD | KONEC>

    // ověříme, že byl zadán buď soubor se zdroji nebo url zdroje
    if (!((inProgParams->feedUrlFlag == true && inProgParams->feedFileFlag == false)
          || (inProgParams->feedUrlFlag == false && inProgParams->feedFileFlag == true)))
    {
       fprintf(stderr, "\nChyba: Musíte specifikovat buď url zdroje a nebo soubor obsahující seznam url zdrojů!\n");
       fprintf(stderr, "Nápověda: zadejte parametr -h pro výpis nápovědy!\n");
       return false;
    }

    return true;
}

/**
 * Funkce která na základě vstupních parametrů naplní seznam URL zdrojů
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @param listOfFeeds výstupní parametr, který představuje seznam URL zdrojů
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool fillListOfFeeds(InputProgramParams inProgParams, vector<string>* listOfFeeds) {

    // pokud nebyl specifikován soubor se zdroji, ale byl specifikován pouze jeden zdroj, uložíme jej do seznamu zdrojů
    if (inProgParams.feedFileFlag == false) {
        (*listOfFeeds).push_back(inProgParams.urlOfFeed);
    }
    // pokud byl specifikován soubor se zdroji, tak zdroje postupně načteme do seznamu zdrojů
    else {

        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // © 2009-2015 Jan-Philip Gehrcke
        // Titulek: Reading files line by line in C++ using ifstream: dealing correctly with badbit, failbit, eofbit, and perror()
        // Datum publikování: June 25, 2011
        // Dostupné z: https://gehrcke.de/2011/06/reading-files-in-c-using-ifstream-dealing-correctly-with-badbit-failbit-eofbit-and-perror/
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        string line;
        ifstream file (inProgParams.feedFile);

        // otevřeme soubor se zdroji
        if (!file.is_open()) {
            fprintf(stderr, "\nChyba při otvírání souboru %s\n", inProgParams.feedFile.c_str());
            perror("Detail chyby: ");
            return false;
        }

        // načítáme zdroje po řádcích a ukládáme do seznamu listOfFeeds
        while(getline(file, line)) {
            if (!line.empty() && line.substr(0,1) != "#") {
                (*listOfFeeds).push_back(line);
            }
        }

        // pokud se během operace čtení vyskytla chyba
        if (file.bad()) {
            fprintf(stderr, "\nChyba při čtení souboru %s\n", inProgParams.feedFile.c_str());
            perror("Detail chyby: ");
            return false;
        }

        // <PŘEVZATÝ KÓD | KONEC>
    }

    return true;
}
