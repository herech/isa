/*
 * Soubor:    feed.cpp
 * Vytvořeno: 7. listopad 2015, 20:06
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce pro práci se staženými zdroji ve formátu Atom/RSS (zejména jejich parsování a získání formátovaných informací)
 */

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include "network.h"
#include "input.h"
#include "feed.h"
#include "utility.h"

using namespace std;

bool parseAtomFeed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedString, InputProgramParams inProgParams);

bool parseAtomEntryElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *outputFormattedString, InputProgramParams inProgParams);

bool parseRSSv1Feed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedString, InputProgramParams inProgParams);

bool parseRSSv1ItemElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *outputFormattedString, InputProgramParams inProgParams);

bool parseRSSv2Feed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedString, InputProgramParams inProgParams);

bool parseRSSv2ItemElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *outputFormattedString, InputProgramParams inProgParams);

void genericErrorFuncHandler (void * ctx, const char * msg, ...);

void structuredErrorFuncHandler	(void * userData, xmlErrorPtr error);

/**
 * Funkce zpracuje obsah daného zdroje novinek a vrátí řetězec obsahující formátované požadované informace z daného zdroje.
 * Pro zpracování jednotlivých formátů zdrojů novinek volá dedikované funkce.
 * @param xmlContent obsah zdroje novinek ve formátu XML
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param outputFormattedStringForFeed výstupní parametr, přes který funkce vrací formátovaný řetězec,
                                       obsahující formátované požadované informace z daného zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseFeed (string xmlContent, Url url, string *outputFormattedStringForFeed, InputProgramParams inProgParams) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    xmlDocPtr xmlFeedTree;  // ukazatel na DOM model dokumentu
    xmlNodePtr xmlFeedNode; // ukazatel na uzel DOM

    // nastavíme funkce pro ošetřování vzniklých chyb (funkce vzniklé chyby ignorují, aplikace provádí vlastní výpis chyb)
    xmlSetGenericErrorFunc		(NULL, genericErrorFuncHandler);
    xmlSetStructuredErrorFunc	(NULL, structuredErrorFuncHandler);

    // načteme DOM model XML dokumentu
    xmlFeedTree = xmlParseMemory(xmlContent.c_str(), xmlContent.length());

    // pokud došlo při parsování XML k chybě
    if (xmlFeedTree == NULL ) {
        fprintf(stderr, "\nChyba: zdroj novinek se nepodařilo načíst pomocí knihovny libxml2.\n");
        fprintf(stderr, "Detail chyby: zdroj novinek není ve validním formátu XML\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
    }

    // získáme tzv. kořenový element
    xmlFeedNode = xmlDocGetRootElement(xmlFeedTree);

    // pokud neexistuje kořenový element
    if (xmlFeedNode == NULL) {
        fprintf(stderr, "\nChyba: zdroj není validní, protože je to prázdný dokument\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        xmlFreeDoc(xmlFeedTree);
        return false;
    }

    // jedná se zdroj ve formátu ATOM? Pak jej zpracujeme pomocí dedikované funkce
    if (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *) "feed")) {
        return parseAtomFeed (xmlFeedTree, xmlFeedNode, xmlContent, url, outputFormattedStringForFeed, inProgParams);
    }
    // nebo se jedná o zdroj ve formátu RSS 1.0? Pak jej zpracujeme pomocí dedikované funkce
    else if (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *) "RDF")
            && xmlFeedNode->ns
            && (!xmlStrcmp(xmlFeedNode->ns->href, (const xmlChar *)"http://www.w3.org/1999/02/22-rdf-syntax-ns#"))) {

        return parseRSSv1Feed (xmlFeedTree, xmlFeedNode, xmlContent, url, outputFormattedStringForFeed, inProgParams);
    }
    // nebo se jedná o zdroj ve formátu RSS 2.0? Pak jej zpracujeme pomocí dedikované funkce
    else if (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *) "rss")) {
        return parseRSSv2Feed (xmlFeedTree, xmlFeedNode, xmlContent, url, outputFormattedStringForFeed, inProgParams);
    }
    // jedná se o jiný typ XML dokumentu, respektive zdroj novinek není v podporovaném formátu
    else {
        fprintf(stderr, "\nChyba: zdroj není v žádném z podporovaných formátů (Atom, RSS 1.0, RSS 2.0), protože název kořenového elementu se s žádným z těchto formátů neslučuje\n");
        fprintf(stderr, "Detail chyby: název kořenového elementu je: %s\n", string((char *)xmlFeedNode->name).c_str());
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        xmlFreeDoc(xmlFeedTree);
        return false;
    }

    // <PŘEVZATÝ KÓD | KONEC>

}

/**
 * Funkce zpracuje obsah zdroje novinek ve formátu ATOM a vrátí řetězec obsahující formátované požadované informace z daného zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě kořenový element)
 * @param xmlContent obsah zdroje novinek ve formátu XML
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param outputFormattedStringForFeed výstupní parametr, přes který funkce vrací formátovaný řetězec,
                                       obsahující formátované požadované informace z daného zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseAtomFeed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedStringForFeed, InputProgramParams inProgParams) {

    xmlChar *title;                 // hodnota elementu title
    bool titleFoundFlag = false;    // je titulek zdroje obsažen ve zdroji?
    int numberOfParsedEntries = 0;  // počet doposud zpracovaných záznamů

    string titleStr = "";   // řetězec reprezentující titulek zdroje
    string entriesStr = ""; // řetězec reprezentující formátované informace o záznamech zdroje

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    xmlFeedNode = xmlFeedNode->xmlChildrenNode; // posuneme se z kořenového elementu na jeho prvního potomka

    // projdeme synovské uzly kořenového elementu a najdeme požadované elementy a uložíme si jejich obsah
	while (xmlFeedNode != NULL) {
        // jedná se o element title?
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"title"))) {
			// zjistíme titulek
			title = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element title nepárový?
            if (title == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože jeho element title není párový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                xmlFreeDoc(xmlFeedTree);
                return false;
            }
            // uložíme si daný titulek
			titleStr += "*** " + trim(string((char *)title)) + " ***\n";
			xmlFree(title);
			titleFoundFlag = true;
		}
		// jedná se o element entry?
		else if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"entry"))) {
            // pokud byl zadán jeden z dodatečných atributů oddělíme jednotlivé záznamy prázdným řádkem
            if (numberOfParsedEntries > 0 && (inProgParams.showUpdatedFlag || inProgParams.showAuthorFlag || inProgParams.showRelatedUrlFlag)) {
                entriesStr += "\n";
            }
            // element entry (tedy záznam) zpracujeme dedikovanou funkcí
            if (!parseAtomEntryElement (xmlFeedTree, xmlFeedNode, url, &entriesStr, inProgParams)) {
                xmlFreeDoc(xmlFeedTree);
                return false;
            }
            // pokud uživatel chce získat informace ze zdrojů jen pro nejnovější záznamy
            if (inProgParams.showNewestRecordFlag == true) {
                break;
            }
            numberOfParsedEntries++;
		}

        xmlFeedNode = xmlFeedNode->next;
	}

	xmlFreeDoc(xmlFeedTree);

    // <PŘEVZATÝ KÓD | KONEC>

	// zdroj neobsahuje titulek, to je chyba!
	if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože neobsahuje titulek zdroje (title)\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}

    // uložíme naformátované informace o daném zdroji do výstupního parametru
    *outputFormattedStringForFeed = titleStr + entriesStr;
    return true;
}

/**
 * Funkce zpracuje element entry v rámci zdroje novinek ve formátu ATOM
 * a vrátí řetězec obsahující formátované požadované informace z daného záznamu zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě element entry - záznam)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param entriesStr výstupní parametr, přes který funkce vrací formátovaný řetězec,
                     obsahující formátované požadované informace ze záznamu zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseAtomEntryElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *entriesStr, InputProgramParams inProgParams) {

    xmlChar *title;       // hodnota elementu title
    xmlChar *updated;     // hodnota elementu updated
    xmlChar *author;      // hodnota elementu author
    xmlChar *relatedUrl;  // alias pro hodnotu elementu link
    xmlChar *typeOfUrl;   // hodnota atributu rel v elementu link

    bool titleFoundFlag = false; // je titulek záznamu obsažen v záznamu?

    string titleStr = "";   // řetězec reprezentující titulek záznamu
    string authorStr = "";  // řetězec reprezentující autora záznamu
    string updatedStr = ""; // řetězec reprezentující datum poslední změny záznamu
    string linkStr = "";    // řetězec reprezentující odkaz na URL verzi záznamu

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // projdeme synovské uzly elementu entry a najdeme požadované elementy a uložíme si jejich obsah
    xmlFeedNode = xmlFeedNode->xmlChildrenNode;
    while (xmlFeedNode != NULL) {

        // jedná se o element title?
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"title"))) {
            // zjistíme titulek
            title = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element title nepárový?
            if (title == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element title nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si daný titulek
            titleStr += trim(string((char *)title)) + "\n";
			xmlFree(title);
			titleFoundFlag = true;
        }
        // nebo se jedná o element author?
        else if (inProgParams.showAuthorFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"author"))) {
            xmlNodePtr xmlFeedNodeAuthor = xmlFeedNode->xmlChildrenNode; // najdeme si prvního potomka elementu author
            // projdeme synovské uzly elementu author a najdeme požadované elementy a uložíme si jejich obsah
            while (xmlFeedNodeAuthor != NULL) {

                // jedná se o element name?
                if ((!xmlStrcmp(xmlFeedNodeAuthor->name, (const xmlChar *)"name"))) {
                    // zjistíme jméno autora
                    author = xmlNodeListGetString(xmlFeedTree, xmlFeedNodeAuthor->xmlChildrenNode, 1);

                    // je element name nepárový?
                    if (author == NULL) {
                        fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element autor nepárový\n");
                        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                        return false;
                    }
                    // uložíme si jméno autora
                    authorStr += "Autor: " + trim(string((char *)author)) + "\n";
                    xmlFree(author);
                }
                xmlFeedNodeAuthor = xmlFeedNodeAuthor->next;
            }
        }
        // nebo se jedná o element updated?
        else if (inProgParams.showUpdatedFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"updated"))) {
            // zjistíme datum poslední změny záznamu
            updated = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element updated nepárový?
            if (updated == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element updated nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si datum poslední změny záznamu
            updatedStr += "Aktualizace: " + trim(string((char *)updated)) + "\n";
			xmlFree(updated);
        }
        // nebo se jedná o element link?
        else if (inProgParams.showRelatedUrlFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"link"))) {
            // získáme informaci o typu odkazu
            typeOfUrl = xmlGetProp(xmlFeedNode, (const xmlChar *)"rel");

            // má element link atribut rel s hodnotou alternate? (pokud tento atribut nemá, je hodnotoa alternate implicitní)
            if (typeOfUrl == NULL || (!xmlStrcmp(typeOfUrl, (const xmlChar *)"alternate"))) {
                // zjistíme související url záznamu
                relatedUrl = xmlGetProp(xmlFeedNode, (const xmlChar *)"href");

                // pokud element link nemá atribut href, který by obsahoval související url
                if (relatedUrl == NULL) {
                    fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některého jeho záznamu postrádá element link atribut href\n");
                    fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                    return false;
                }
                // uložíme si související url záznamu
                linkStr += "URL: " + trim(string((char *)relatedUrl)) + "\n";
                xmlFree(relatedUrl);
            }
			xmlFree(typeOfUrl);
        }
        xmlFeedNode = xmlFeedNode->next;
    }

    // <PŘEVZATÝ KÓD | KONEC>

    // pokud nebyl v rámci záznamu nalezen titulek jedná se o chybu
    if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý záznam neobsahuje titulek (title)\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}

    // uložím si formátovanou informaci o daném záznamu
	*entriesStr += titleStr + authorStr + updatedStr + linkStr;
    return true;

}

/**
 * Funkce zpracuje obsah zdroje novinek ve formátu RSS 1.0 a vrátí řetězec obsahující formátované požadované informace z daného zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě kořenový element)
 * @param xmlContent obsah zdroje novinek ve formátu XML
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param outputFormattedStringForFeed výstupní parametr, přes který funkce vrací formátovaný řetězec,
                                       obsahující formátované požadované informace z daného zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseRSSv1Feed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedStringForFeed, InputProgramParams inProgParams) {

    xmlNodePtr xmlChannelNode; // uzel pro element channel a jeho potomky

    xmlChar *title;                 // hodnota elementu title
    bool titleFoundFlag = false;    // je titulek zdroje obsažen ve zdroji?
    int numberOfParsedEntries = 0;  // počet doposud zpracovaných záznamů

    string titleStr = "";    // řetězec reprezentující titulek zdroje
    string entriesStr = "";  // řetězec reprezentující formátované informace o záznamech zdroje

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    xmlFeedNode = xmlFeedNode->xmlChildrenNode; // posuneme se z kořenového elementu na jeho prvního potomka

    // projdeme synovské uzly kořenového elementu a najdeme požadované elementy a uložíme si jejich obsah
	while (xmlFeedNode != NULL) {

        // Pokud jsme narazili na element channel, prohledáme jej kvůli elementu title
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"channel"))){
            // posuneme se z elementu channel na jeho prvního potomka
            xmlChannelNode = xmlFeedNode->xmlChildrenNode;
            // projdeme synovské uzly elementu channel a najdeme požadované elementy a uložíme si jejich obsah
            while (xmlChannelNode != NULL) {

                // jedná se o element title?
                if ((!xmlStrcmp(xmlChannelNode->name, (const xmlChar *)"title"))){
                    // zjistíme titulek
                    title = xmlNodeListGetString(xmlFeedTree, xmlChannelNode->xmlChildrenNode, 1);

                    // je element title nepárový?
                    if (title == NULL) {
                        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 1.0, protože jeho element title není párový\n");
                        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                        xmlFreeDoc(xmlFeedTree);
                        return false;
                    }
                    // uložíme si daný titulek
                    titleStr += "*** " + trim(string((char *)title)) + " ***\n";
                    xmlFree(title);
                    titleFoundFlag = true;
                }
                xmlChannelNode = xmlChannelNode->next;
            }
		}
		// jedná se o element item?
		else if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"item"))) {
            // pokud byl zadán jeden z dodatečných atributů oddělíme jednotlivé záznamy prázdným řádkem
            if (numberOfParsedEntries > 0 && (inProgParams.showUpdatedFlag || inProgParams.showAuthorFlag || inProgParams.showRelatedUrlFlag)) {
                entriesStr += "\n";
            }
            // element item (tedy záznam) zpracujeme dedikovanou funkcí
            if (!parseRSSv1ItemElement (xmlFeedTree, xmlFeedNode, url, &entriesStr, inProgParams)) {
                xmlFreeDoc(xmlFeedTree);
                return false;
            }
            // pokud uživatel chce získat informace ze zdrojů jen pro nejnovější záznamy
            if (inProgParams.showNewestRecordFlag == true) {
                break;
            }
            numberOfParsedEntries++;
		}

        xmlFeedNode = xmlFeedNode->next;
	}

	xmlFreeDoc(xmlFeedTree);

	// <PŘEVZATÝ KÓD | KONEC>

    // zdroj neobsahuje titulek, to je chyba
	if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože neobsahuje titulek zdroje (title)\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}

    // uložíme naformátované informace o daném zdroji do výstupního parametru
    *outputFormattedStringForFeed = titleStr + entriesStr;
    return true;
}

/**
 * Funkce zpracuje element item v rámci zdroje novinek ve formátu RSS 1.0
 * a vrátí řetězec obsahující formátované požadované informace z daného záznamu zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě element entry - záznam)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param entriesStr výstupní parametr, přes který funkce vrací formátovaný řetězec,
                     obsahující formátované požadované informace ze záznamu zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseRSSv1ItemElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *entriesStr, InputProgramParams inProgParams) {

    xmlChar *title;       // hodnota elementu title
    xmlChar *updated;     // hodnota elementu updated
    xmlChar *author;      // hodnota elementu author
    xmlChar *relatedUrl;  // alias pro hodnotu elementu link

    bool titleFoundFlag = false; // je titulek záznamu obsažen v záznamu?

    string titleStr = "";    // řetězec reprezentující titulek záznamu
    string authorStr = "";   // řetězec reprezentující autora záznamu
    string updatedStr = "";  // řetězec reprezentující datum poslední změny záznamu
    string linkStr = "";     // řetězec reprezentující odkaz na URL verzi záznamu

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // projdeme synovské uzly elementu ITEM a najdeme požadované elementy a uložíme si jejich obsah
    xmlFeedNode = xmlFeedNode->xmlChildrenNode;
    while (xmlFeedNode != NULL) {

        // jedná se o element title?
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"title"))) {
            // zjistíme titulek
            title = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element title nepárový?
            if (title == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.1, protože některý jeho záznam má element title nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si daný titulek
            titleStr += trim(string((char *)title)) + "\n";
			xmlFree(title);
			titleFoundFlag = true;
        }
        // nebo se jedná o element creator z jmenného prostoru s URI http://purl.org/dc/elements/1.1/?
        else if (inProgParams.showAuthorFlag
                 && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"creator"))
                 && xmlFeedNode->ns
                 && (!xmlStrcmp(xmlFeedNode->ns->href, (const xmlChar *)"http://purl.org/dc/elements/1.1/"))) {

            // zjistíme jméno autora
            author = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element author nepárový?
            if (author == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element creator nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si jméno autora
            authorStr += "Autor: " + trim(string((char *)author)) + "\n";
            xmlFree(author);
        }
        // nebo se jedná o element date z jmenného prostoru s URI http://purl.org/dc/elements/1.1/?
        else if (inProgParams.showUpdatedFlag
                 && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"date"))
                 && xmlFeedNode->ns
                 && (!xmlStrcmp(xmlFeedNode->ns->href, (const xmlChar *)"http://purl.org/dc/elements/1.1/"))) {

            // zjistíme datum poslední změny záznamu
            updated = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element date nepárový?
            if (updated == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element date nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si datum poslední změny záznamu
            updatedStr += "Aktualizace: " + trim(string((char *)updated)) + "\n";
			xmlFree(updated);
        }
        // nebo se jedná o element link?
        else if (inProgParams.showRelatedUrlFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"link"))) {
            // zjistíme související url záznamu
            relatedUrl = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element link nepárový?
            if (relatedUrl == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 1.0, protože u některý jeho záznam má element link nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si související url záznamu
            linkStr += "URL: " + trim(string((char *)relatedUrl)) + "\n";
            xmlFree(relatedUrl);
        }
        xmlFeedNode = xmlFeedNode->next;
    }

    // <PŘEVZATÝ KÓD | KONEC>

    // pokud nebyl v rámci záznamu nalezen titulek jedná se o chybu
    if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 1.0, protože některý záznam neobsahuje titulek (title)\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}

    // uložím si formátovanou informaci o daném záznamu
	*entriesStr += titleStr + authorStr + updatedStr + linkStr;
    return true;
}

/**
 * Funkce zpracuje obsah zdroje novinek ve formátu RSS 2.0 a vrátí řetězec obsahující formátované požadované informace z daného zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě kořenový element)
 * @param xmlContent obsah zdroje novinek ve formátu XML
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param outputFormattedStringForFeed výstupní parametr, přes který funkce vrací formátovaný řetězec,
                                       obsahující formátované požadované informace z daného zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseRSSv2Feed (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode,
                    string xmlContent, Url url, string *outputFormattedStringForFeed, InputProgramParams inProgParams)
{
    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    xmlFeedNode = xmlFeedNode->xmlChildrenNode; // posuneme se z kořenového elementu na jeho prvního potomka

    // pokud kořenový element nemá potomka, je to chyba
    if (xmlFeedNode == NULL) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože neobsahuje jako potomka element <channel>\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        xmlFreeDoc(xmlFeedTree);
        return false;
    }

    bool channelFound = false; // příznak indikující jestli byl nalezen element channel
    // projdeme synovské uzly kořenového elementu, abychom našli element channel
    while (xmlFeedNode != NULL) {
         // jedná se o element channel?
        if (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *) "channel")) {
            channelFound = true;
            break;
        }
        xmlFeedNode = xmlFeedNode->next;
    }

    // pokud jsme element channel nenašli, je to chyba
    if (channelFound == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože kořenový element neobsahuje jako potomka element s názevem <channel>\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        xmlFreeDoc(xmlFeedTree);
        return false;
    }

    xmlChar *title;                 // hodnota elementu title
    bool titleFoundFlag = false;    // je titulek zdroje obsažen ve zdroji?
    int numberOfParsedEntries = 0;  // počet doposud zpracovaných záznamů

    string titleStr = "";    // řetězec reprezentující titulek zdroje
    string entriesStr = "";  // řetězec reprezentující formátované informace o záznamech zdroje

    xmlFeedNode = xmlFeedNode->xmlChildrenNode; // posuneme se z elementu channel na jeho prvního potomka

    // projdeme synovské uzly elementu channel a najdeme požadované elementy a uložíme si jejich obsah
	while (xmlFeedNode != NULL) {
        // jedná se o element title?
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"title"))){
			// zjistíme titulek
			title = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element title nepárový?
            if (title == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože jeho element <title> není párový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                xmlFreeDoc(xmlFeedTree);
                return false;
            }
            // uložíme si daný titulek
			titleStr += "*** " + trim(string((char *)title)) + " ***\n";
			xmlFree(title);
			titleFoundFlag = true;
		}
		// jedná se o element item?
		else if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"item"))) {

            // pokud byl zadán jeden z dodatečných atributů oddělíme jednotlivé záznamy prázdným řádkem
            if (numberOfParsedEntries > 0 && (inProgParams.showUpdatedFlag || inProgParams.showAuthorFlag || inProgParams.showRelatedUrlFlag)) {
                entriesStr += "\n";
            }
            // element item (tedy záznam) zpracujeme dedikovanou funkcí
            if (!parseRSSv2ItemElement (xmlFeedTree, xmlFeedNode, url, &entriesStr, inProgParams)) {
                xmlFreeDoc(xmlFeedTree);
                return false;
            }
            // pokud uživatel chce získat informace ze zdrojů jen pro nejnovější záznamy
            if (inProgParams.showNewestRecordFlag == true) {
                break;
            }
            numberOfParsedEntries++;
		}

        xmlFeedNode = xmlFeedNode->next;
	}

	xmlFreeDoc(xmlFeedTree);

	// <PŘEVZATÝ KÓD | KONEC>

	// zdroj neobsahuje titulek, to je chyba
	if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože neobsahuje titulek zdroje <title>\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}
    // uložíme naformátované informace o daném zdroji do výstupního parametru
    *outputFormattedStringForFeed = titleStr + entriesStr;
    return true;
}

/**
 * Funkce zpracuje element item v rámci zdroje novinek ve formátu RSS 2.0
 * a vrátí řetězec obsahující formátované požadované informace z daného záznamu zdroje.
 * @param xmlFeedTree ukazatel na DOM model dokumentu
 * @param xmlFeedNode ukazatel na uzel DOM (v tomto případě element entry - záznam)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param entriesStr výstupní parametr, přes který funkce vrací formátovaný řetězec,
                     obsahující formátované požadované informace ze záznamu zdroje
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseRSSv2ItemElement (xmlDocPtr xmlFeedTree, xmlNodePtr xmlFeedNode, Url url,
                            string *entriesStr, InputProgramParams inProgParams) {

    xmlChar *title;      // hodnota elementu title
    xmlChar *updated;    // hodnota elementu updated
    xmlChar *author;     // hodnota elementu author
    xmlChar *relatedUrl; // alias pro hodnotu elementu link

    bool titleFoundFlag = false;  // je titulek záznamu obsažen v záznamu?

    string titleStr = "";    // řetězec reprezentující titulek záznamu
    string authorStr = "";   // řetězec reprezentující autora záznamu
    string updatedStr = "";  // řetězec reprezentující datum poslední změny záznamu
    string linkStr = "";     // řetězec reprezentující odkaz na URL verzi záznamu

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © 2002,203 John Fleck <jfleck@inkstain.net>
    // Titulek: Libxml Tutorial
    // Dostupné z: http://www.xmlsoft.org/tutorial/xmltutorial.pdf
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // projdeme synovské uzly elementu item a najdeme požadované elementy a uložíme si jejich obsah
    xmlFeedNode = xmlFeedNode->xmlChildrenNode;
    while (xmlFeedNode != NULL) {

        // jedná se o element title?
        if ((!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"title"))) {
            // zjistíme titulek
            title = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element title nepárový?
            if (title == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože některý jeho záznam má element title nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si daný titulek
            titleStr += trim(string((char *)title)) + "\n";
			xmlFree(title);
			titleFoundFlag = true;
        }
        // nebo se jedná o element author?
        else if (inProgParams.showAuthorFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"author"))) {
            // zjistíme jméno autora
            author = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element author nepárový?
            if (author == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element autor nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si jméno autora
            authorStr += "Autor: " + trim(string((char *)author)) + "\n";
            xmlFree(author);
        }
        // nebo se jedná o element pubDate?
        else if (inProgParams.showUpdatedFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"pubDate"))) {
            // zjistíme datum poslední změny záznamu
            updated = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element pubDate nepárový?
            if (updated == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu Atom, protože některý jeho záznam má element pubDate nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si datum poslední změny záznamu
            updatedStr += "Aktualizace: " + trim(string((char *)updated)) + "\n";
			xmlFree(updated);
        }
        // nebo se jedná o element link?
        else if (inProgParams.showRelatedUrlFlag && (!xmlStrcmp(xmlFeedNode->name, (const xmlChar *)"link"))) {
            // zjistíme související url záznamu
            relatedUrl = xmlNodeListGetString(xmlFeedTree, xmlFeedNode->xmlChildrenNode, 1);

            // je element link nepárový?
            if (relatedUrl == NULL) {
                fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože u některý jeho záznam má element link nepárový\n");
                fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
                return false;
            }
            // uložíme si související url záznamu
            linkStr += "URL: " + trim(string((char *)relatedUrl)) + "\n";
            xmlFree(relatedUrl);
        }
        xmlFeedNode = xmlFeedNode->next;
    }

    // <PŘEVZATÝ KÓD | KONEC>

    // pokud nebyl v rámci záznamu nalezen titulek jedná se o chybu
    if (titleFoundFlag == false) {
        fprintf(stderr, "\nChyba: zdroj není ve validním formátu RSS 2.0, protože některý záznam neobsahuje titulek (title)\n");
        fprintf(stderr, "Detail chyby: zdroj byl načten z url: %s\n", (url.url).c_str());
        return false;
	}

    // uložím si formátovanou informaci o daném záznamu
	*entriesStr += titleStr + authorStr + updatedStr + linkStr;
    return true;
}

// zaregistrované funkce pro ošetřování chyb vzniklýh při parsování XML dokumentu
// (mají prázdné tělo a chyby tedy ignorují, aplikace si totiž chyby sama ošetřuje a vypisuje v jednoduché formě)
void genericErrorFuncHandler (void * ctx, const char * msg, ...) {}

void structuredErrorFuncHandler	(void * userData, xmlErrorPtr error) {}

