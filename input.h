/*
 * Soubor:    input.h
 * Vytvořeno: 31. říjen 2015, 18:16
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce a ADT pro práci se vstupem (načtení a zpracování vstupních parametrů, načtení zdrojů ze souboru, ...)
 */

#ifndef INPUT_PARAMS_H_INCLUDED
#define INPUT_PARAMS_H_INCLUDED

#include <cstdlib>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <vector>


using namespace std;

/**
 * Struktura reprezentující zpracované parametry předané programu
 */
typedef struct {
    string urlOfFeed;           // url zdroje ve formátu Atom
    string feedFile;            // soubor obsahující seznam zdrojů ve formátu Atom
    string certFile;            // soubor obsahující certifikáty, sloužící k ověření platnosti certifikátu SSL/TLS serveru
    string certFileDir;         // složka obsahující soubory s certifikáty

    bool printHelpFlag;         // příznak, který určuje, jestli uživatel chce vypsat nápovědu
    bool feedUrlFlag;           // příznak, který určuje, jestli nám uživatel zadal jako parametr url zdroje ve formátu Atom
    bool feedFileFlag;          // příznak, který určuje, jestli nám uživatel zadal jako parametr soubor obsahující url zdrojů ve formátu Atom
    bool certFileFlag;          // příznak, který určuje, jestli nám uživatel zadal jako parametr soubor obsahující certifikáty
    bool showNewestRecordFlag;  // příznak, který určuje, jestli se má ke každému zdroji vypisovat pouze info k jeho nejnovějšímu záznamu
    bool showUpdatedFlag;       // příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o jeho poslední změně (pokud je dostupná)
    bool showAuthorFlag;        // příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o jeho autorovi (pokud je dostupná)
    bool showRelatedUrlFlag;    // příznak, který určuje, jestli se má ke každému záznamu vypisovat informace o asociovaném URL (pokud je dostupná)
} InputProgramParams;

/**
 * Funkce která zpracuje vstupní parametry a tyto vrací ve struktuře
 * @param argc počet argumentů předaných programu
 * @param argv pole argumentů předaných programu
 * @param inProgParams výstupní parametr, jedná se o strukturu obsahující vstupní parametry programu (deklarovaná v input.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool processTheParams(int argc, char** argv, InputProgramParams *inProgParams);

/**
 * Funkce která na základě vstupních parametrů naplní seznam URL zdrojů
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @param listOfFeeds výstupní parametr, který představuje seznam URL zdrojů
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool fillListOfFeeds(InputProgramParams inProgParams, vector<string>* listOfFeeds);

#endif // INPUT_PARAMS_H_INCLUDED
