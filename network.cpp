/*
 * Soubor:    network.cpp
 * Vytvořeno: 31. říjen 2015, 22:13
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje funkce a ADT pro práci v síťovém prostředí (parsování url, vytváření zabezpečeného a nezabezpečeného spojení, stahování dat, ...)
 */

#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <netdb.h>
#include <string>
#include <errno.h>
#include <strings.h>

#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"

#include "network.h"
#include "input.h"
#include "utility.h"

// velikost bufferu při čtení odpovědi serveru pomocí funkce read
#define READ_BUFFER_SIZE 1024
// maximální hloubka přesměrování (při HTTP kódu 302/301)
#define MAX_REDIRECT_DEEP 10

using namespace std;

// HTTP kódy, které rozpoznává tento program
typedef enum {
    code_301, code_302, code_200, code_unknown
} HTTPCodes;

bool getContentFromReceivedResponse(string serverResponse, string* content, Url url);

bool sendHTTPRequest(int* sockfd, Url url);

bool getHTTPResponse(int* sockfd, Url url, string* newLocation, string* receivedContent);

bool createUnsecureConnectionToServer(int* sockfd, Url url);

bool closeUnsecureConnectionToServer(int* sockfd, Url url);

void closeSecureConnectionToServer(BIO* bio, SSL_CTX * ctx);

bool sendHTTPSRequest(BIO* bio, Url url);

bool getHTTPSResponse(BIO* bio, Url url, string* newLocation, string* receivedContent);

void initOpenSSL();

bool loadCerts(SSL_CTX *ctx, Url url, bool certFileFlag, string certFile, string certFileDir);

bool createSecureConnectionToServer(BIO* bio, Url url);

string getLocation(string httpResponse);

HTTPCodes getHTTPCode(string httpResponse, Url url);

/**
 * Funkce má jako vstupní i výstupní parametr zároveň strukturu Url (deklarovaná v network.h).
 * Před voláním funkce je potřeba nastavit složku url struktury Url a funkce sama nastaví ostatní složky této struktury
 * @param url vstupně/výstupní parametr, kterým je struktura obsahující URL (včetně jeho podčástí) (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool parseUrl(Url* url) {

    // inicializace hodnot struktury
    url->protocol = "";
    url->portNumber = "";
    url->hostName = "";
    url->localResource = "";

    size_t strPosition;  // pozice hledaného prvku v adrese
    size_t portPosition; // pozice portu v adrese URL

    strPosition = (url->url).find("://"); // určíme hranici mezi protokolem a url adresou

    // špatný formát URL, řetězec :// není v URL přítomen
    if (strPosition == string::npos) {
        fprintf(stderr, "\nChyba: URL adresa %s není ve správném formátu\n", (url->url).c_str());
        return false;
    }
    string protocol = (url->url).substr(0,strPosition); // protokol http nebo https

    // zjistíme číslo portu na základě použitého protokolu
    if (strcasecmp(protocol.c_str(), "http") == 0) {
        url->portNumber = "80";
        url->protocol = "http";
    }
    else if(strcasecmp(protocol.c_str(), "https") == 0) {
        url->portNumber = "443";
        url->protocol = "https";
    }
    else {
        fprintf(stderr, "\nChyba: v URL adrese %s nebyl specifikován požadovaný protokol http nebo https\n", (url->url).c_str());
        return false;
    }

    string urlWithoutProtocol = (url->url).substr(strPosition + 3, string::npos); // zbytek adresy url bez protokolu
    strPosition = urlWithoutProtocol.find(":"); // určíme hranici mezi doménovým jménem a portem (pokud je specifikován)

    portPosition = strPosition; // pozice čísla portu

    strPosition = urlWithoutProtocol.find("/"); // určíme hranici mezi doménovým jménem a cestou k lokálnímu zdroji

    // pokud není určen lokální zdroj (tzn. je zadána jen doména), je zdrojem nejspíše indexový soubor
    if (strPosition == string::npos) {
        url->localResource = "/";

        // pokud je číslo portu specifikováno určíme tento port a jméno hosta
        if (portPosition != string::npos) {
            url->portNumber = urlWithoutProtocol.substr(portPosition + 1, string::npos);
            // pokud číslo portu není číslem
            if (!isNonNegativeNumber(url->portNumber)) {
                fprintf(stderr, "\nChyba: v URL adrese %s nebyl zadán port jako číslo\n", (url->url).c_str());
                return false;
            }

            url->hostName = urlWithoutProtocol.substr(0, portPosition);
        }
        // pokud není, určíme jméno hosta
        else {
            url->hostName = urlWithoutProtocol;
        }
    }
    // pokud je určen lokální zdroj je třeba zjistit číslo portu a jméno hosta a cestu k lokálnímu zdroji
    else {

        // pokud je číslo portu specifikováno určíme tento port a jméno hosta
        if (portPosition != string::npos) {
            url->portNumber = urlWithoutProtocol.substr(portPosition + 1, strPosition - (portPosition + 1));
            // pokud číslo portu není číslem
            if (!isNonNegativeNumber(url->portNumber)) {
                fprintf(stderr, "\nChyba: v URL adrese %s nebyl zadán port jako nezáporné číslo\n", (url->url).c_str());
                return false;
            }

            url->hostName = urlWithoutProtocol.substr(0, portPosition);
        }
        // pokud není, určíme jméno hosta
        else {
            url->hostName = urlWithoutProtocol.substr(0, strPosition);
        }

        // určíme cestu k lokálnímu zdroji
        url->localResource = urlWithoutProtocol.substr(strPosition, string::npos);
    }

    return true;
}

/**
 * Funkce vrátí obsah zdroje na dané URL adrese (tělo HTTP(S) odpovědi serveru)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param inProgParams struktura obsahující vstupní parametry programu (deklarovaná v input.h)
 * @param content výstupní parametr, přes který funkce vrací řetězec, který reprezentuje obsah zdroje
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool getContentFromServer(Url url, InputProgramParams inProgParams, string* content, int deep) {

    // nejdřív ověříme, kolikrát již byla tato funkce rekurzivně zavolána (v rámci přesměrování pomocí kód 301/302)
    // pokud je to více jak MAX_REDIRECT_DEEP přesměrování tak už to asi není v pořádku
    if (deep > MAX_REDIRECT_DEEP) {
        fprintf(stderr, "\nChyba: pravděpodobně vznikla směrovací smyčka (> 10 přesměrování pomocí kódu 301)");
        fprintf(stderr, "Detail chyby: poslední adresa URL pro kterou došlo k překročení limitu 10 směrování: %s\n", (url.url).c_str());
        return false;
    }

    string newLocation = "";    // nová lokace v případě přesměrování pomocí kódu 301
    string serverResponse = ""; // odpověď serveru na žádost o data z daného zdroje

    // pokud je protokolem HTTP
    if (url.protocol == "http") {
        int sockfd;  // deskriptor klientského socketu

        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // Kód převzat z vlastního projektu IPK
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        // vytvoříme nezabezpečené spojení
        if (createUnsecureConnectionToServer(&sockfd, url) == false) {
            return false;
        }

        // zašleme serveru HTTP požadavek pro získání obsahu zdroje na dané URL adrese
        if (sendHTTPRequest(&sockfd, url) == false) {
            closeUnsecureConnectionToServer(&sockfd, url);
            return false;
        }

        // získáme od serveru HTTP odpověď, která bude obsahovat mj. obsah zdroje
        if (getHTTPResponse(&sockfd, url, &newLocation, &serverResponse) == false) {
            // pokud je důvodem "chyby" to, že je potřeba provést přesměrování kvůli HTTP kódu 301
            // tak se rekurzivně zanoříme, ale tentokrát pro novou url adresu
            if (newLocation != "") {
                url.url = newLocation;

                // rozkouskujeme url
                if (parseUrl(&url) == false) {
                    return false;
                }
                // uzavřeme nezabezpečené spojení
                if (closeUnsecureConnectionToServer(&sockfd, url) == false) {
                    return false;
                }
                // rekurzivní volání pro nové url, na které nás přesměrovává HTTP odpověď serveru
                return getContentFromServer(url, inProgParams, content, ++deep);

            }
            // uzavřeme nezabezpečené spojení
            closeUnsecureConnectionToServer(&sockfd, url);
            return false;
        }

        // uzavřeme nezabezpečené spojení
        if (closeUnsecureConnectionToServer(&sockfd, url) == false) {
            return false;
        }

        // <PŘEVZATÝ KÓD | KONEC>
    }
    // jinak vytvoříme zabezpečené spojení
    else {
        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // © Kenneth Ballard, Software Engineer, MediNotes Corp.
        // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
        // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
        // Datum publikování: 28 June 2012 (First published 22 July 2004)
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        //inicializujeme OpenSSL
        initOpenSSL();

        // vytvoříme pomocné struktury
        BIO * bio;
        SSL_CTX * ctx = SSL_CTX_new(SSLv23_client_method());
        SSL * ssl;

        // načteme certifikáty
        if (loadCerts(ctx, url, inProgParams.certFileFlag, inProgParams.certFile, inProgParams.certFileDir) == false) {
            SSL_CTX_free(ctx);
            return false;
        }

        // inicializujeme pomocné struktury
        bio = BIO_new_ssl_connect(ctx);
        BIO_get_ssl(bio, & ssl);
        SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // Kód převzat z vlastního projektu IPK
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        // pokusíme se připojit k serveru a případně ošetříme chybu
        if (createSecureConnectionToServer(bio, url) == false) {
            closeSecureConnectionToServer(bio, ctx);
            return false;
        }

        // ověříme platnost certifikátů
        if(SSL_get_verify_result(ssl) != X509_V_OK) {
            fprintf(stderr, "\nChyba: nepodařilo se ověřit certifikát serveru: %s\n", (url.hostName).c_str());
            fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
            closeSecureConnectionToServer(bio, ctx);
            return false;
        }

        // zašleme serveru HTTP over SSL požadavek pro získání obsahu zdroje na dané URL adrese
        if (sendHTTPSRequest(bio, url) == false) {
            closeSecureConnectionToServer(bio, ctx);
            return false;
        }

        // získáme od serveru HTTP over SSL odpověď, která bude obsahovat mj. obsah zdroje
        if (getHTTPSResponse(bio, url, &newLocation, &serverResponse) == false) {
            // pokud je důvodem "chyby" to, že je potřeba provést přesměrování kvůli HTTP kódu 301
            // tak se rekurzivně zanoříme, ale tentokrát pro novou url adresu
            if (newLocation != "") {
                url.url = newLocation;

                if (parseUrl(&url) == false) {
                    return false;
                }
                // Uzavřeme zabezpečené spojení
                closeSecureConnectionToServer(bio, ctx);
                // rekurzivní volání pro nové url, na které nás přesměrovává HTTP over SSL odpověď serveru
                return getContentFromServer(url, inProgParams, content, ++deep);
            }
            // Uzavřeme zabezpečené spojení
            closeSecureConnectionToServer(bio, ctx);
            return false;
        }
        // Uzavřeme zabezpečené spojení
        closeSecureConnectionToServer(bio, ctx);

        // <PŘEVZATÝ KÓD (IPK) | KONEC>
        // <PŘEVZATÝ KÓD (Kenneth Ballard) | KONEC>

    }

    // vrátíme přes ukazatel obsah z http odpovědi serveru
    // pokud při pokusu o získání obsahu z odpovědi serveru dojde k chybě, kvůli špatnému formátu odpovědi serveru
    if (getContentFromReceivedResponse(serverResponse, content, url) == false) {
        return false;
    }

    return true;
}

/**
 * Funkce vrátí tělo (obsah) z HTTP(S) odpovědi
 * @param serverResponse surová HTTP(S) odpověď serveru
 * @param content výstupní parametr, přes který funkce vrací tělo HTTP(S) odpovědi
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool getContentFromReceivedResponse(string serverResponse, string* content, Url url) {
    // konec hlavičky HTTP odpovědi
    size_t headerResponseEnd = serverResponse.find("\r\n\r\n");
    // pokud nelze najít konec hlavičky, tak se nejedná o validní HTTP odpoveď
    if (headerResponseEnd == string::npos) {
        fprintf(stderr, "\nChyba: nepodařilo se získat obsah z odpovědi serveru pro zdroj: %s%s\n", (url.hostName).c_str(), (url.localResource).c_str());
        fprintf(stderr, "Detail chyby: odpověď serveru má nesprávný formát\n");
        return false;
    }

    // přes výstupní parametr předám tělo HTTP odpovědi
    *content = serverResponse.substr(headerResponseEnd + 4, string::npos);

    return true;
}

/**
 * Funkce vytvoří nezabezpečené připojení k serveru a vrátí deskriptor socketu pro toto připojení
 * @param sockfd výstupní parametr, přes který funkce vrací deskriptor socketu pro nové připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool createUnsecureConnectionToServer(int* sockfd, Url url) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    struct hostent *hptr;   // struktua obsahující mj. IP adresu serveru
    struct sockaddr_in sin; // struktua obsahující informace, důležité pro komunikaci mezi klientem a serverem

    // vytvoříme socket
    if ((*sockfd = socket(AF_INET, SOCK_STREAM, 0 )) < 0) {
        // chyba při vytváření socketu
        fprintf(stderr, "\nChyba: nepodařilo se vytvořit socket pro server: %s\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        return false;
    }

    sin.sin_family = AF_INET;                             // nastavíme protokol
    sin.sin_port = htons(atoi(url.portNumber.c_str()));   // nastavíme číslo portu serveru

    // zkusíme přeložit jméno serveru na ip adresu
    if ( (hptr =  gethostbyname(url.hostName.c_str())) == NULL) {
        // překlad doménového jména selhal, ukončíme program
        fprintf(stderr, "\nChyba: nepodařilo se přeložit doménové jméno: %s na IP adresu\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        closeUnsecureConnectionToServer(sockfd, url);
        return false;
    }

    // nastavíme ip adresu serveru
    memcpy( &sin.sin_addr, hptr->h_addr, hptr->h_length);

    // pokusíme se připojit k serveru
    if (connect (*sockfd, (struct sockaddr *)&sin, sizeof(sin) ) < 0 ) {
        // pokud se připojení nezdařilo
        fprintf(stderr, "\nChyba: nepodařilo se vytvořit nezabezpečené připojení k serveru: %s\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        closeUnsecureConnectionToServer(sockfd, url);
        return false;
    }

    return true;

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce vytvoří zabezpečené připojení k serveru a vrátí deskriptor BIO pro toto připojení
 * @param bio výstupní parametr, přes který funkce vrací deskriptor BIO pro nové připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool createSecureConnectionToServer(BIO* bio, Url url) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © Kenneth Ballard, Software Engineer, MediNotes Corp.
    // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
    // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
    // Datum publikování: 28 June 2012 (First published 22 July 2004)
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // pokusíme se připojit k serveru
    BIO_set_conn_hostname(bio, (url.hostName + ":" + url.portNumber).c_str());

    // ověříme jestli se spojení provedlo a provedeme handshake
    if(BIO_do_connect(bio) <= 0)
    {
        fprintf(stderr, "\nChyba: nepodařilo se vytvořit zabezpečené připojení k serveru: %s\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        return false;
    }

    return true;

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce uzavře zabezpečené připojení k serveru
 * @param bio deskriptor BIO pro dané připojení
 * @param ctx SSL kontext
 */
void closeSecureConnectionToServer(BIO* bio, SSL_CTX * ctx) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © Kenneth Ballard, Software Engineer, MediNotes Corp.
    // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
    // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
    // Datum publikování: 28 June 2012 (First published 22 July 2004)
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // uzavřeme spojení (zničíme strukturu bio a s ní související sockety + strukturu ctx - uchovávající SSL kontext)
    BIO_free_all(bio);
    SSL_CTX_free(ctx);

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce uzavře nezabezpečené připojení k serveru
 * @param sockfd deskriptor socketu pro dané připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool closeUnsecureConnectionToServer(int* sockfd, Url url) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // uzavřeme socket
    if (close(*sockfd) < 0) {
        // pokud uzavření selhalo
        fprintf(stderr, "\nChyba: nepodařilo se uzavřít spojení pro server: %s\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        return false;
    }
    return true;

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce odešle HTTP GET požadavek na server, který žádá o poslání obsahu zdroje na dané URL
 * @param sockfd deskriptor socketu pro dané připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool sendHTTPRequest(int* sockfd, Url url) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // HTTP GET požadavek pro získání obsahu zdroje na dané url adrese
    string messageToSend;
    messageToSend += "GET " + url.localResource + " HTTP/1.0" + "\r\n";
    messageToSend += "Host: " + url.hostName + ":" + url.portNumber + "\r\n";
    messageToSend += "User-Agent: My User Agent\r\n";
    messageToSend += "\r\n";

    int bytes_sent; // proměnná, která určuje kolik bajtů poslala funkce write

    // postupně odešleme celý požadavek na server
    while(1) {
        // pokusíme se odeslat všechna data na server
        if ((bytes_sent = write(*sockfd, messageToSend.c_str(), messageToSend.length())) <= 0 ) {
            // chyba při odesílání dat
            fprintf(stderr, "\nChyba: nepodařilo se odeslat HTTP požadavek na server: %s\n", (url.hostName).c_str());
            fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
            fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
            return false;
        }
        // pokud z nějakého důvodu nemohla být všechna data zaslána na server,
        // připravíme poslání zbytku dat pro další iteraci
        if ((unsigned int) bytes_sent != messageToSend.length()) {
            messageToSend = messageToSend.substr(bytes_sent, string::npos);
        }
        // pokud byla všechna data úspěšeně odeslána vyskočíme ze smyčky
        else {
            break;
        }
    }

    return true;

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce odešle HTTP over SSL GET požadavek na server, který žádá o poslání obsahu zdroje na dané URL
 * @param bio BIO deskriptor pro dané připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool sendHTTPSRequest(BIO* bio, Url url) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // HTTP GET požadavek pro získání obsahu zdroje na dané url adrese
    string messageToSend;
    messageToSend += "GET " + url.localResource + " HTTP/1.0" + "\r\n";
    messageToSend += "Host: " + url.hostName + ":" + url.portNumber + "\r\n";
    messageToSend += "User-Agent: My User Agent\r\n";
    messageToSend += "\r\n";

    int bytes_sent; // proměnná, která určuje kolik bajtů poslala funkce BIO_write

    // postupně odešleme celý požadavek na server
    while(1) {
        // pokusíme se odeslat všechna data na server

        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // © Kenneth Ballard, Software Engineer, MediNotes Corp.
        // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
        // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
        // Datum publikování: 28 June 2012 (First published 22 July 2004)
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        if ((bytes_sent = BIO_write(bio, messageToSend.c_str(), messageToSend.length())) <= 0 ) {

        // <PŘEVZATÝ KÓD (Kenneth Ballard) | KONEC>

            // chyba při odesílání dat
            fprintf(stderr, "\nChyba: nepodařilo se odeslat HTTPS požadavek na server: %s\n", (url.hostName).c_str());
            fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
            fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));

            return false;
        }
        // pokud z nějakého důvodu nemohla být všechna data zaslána na server,
        // připravíme poslání zbytku dat pro další iteraci
        if ((unsigned int) bytes_sent != messageToSend.length()) {
            messageToSend = messageToSend.substr(bytes_sent, string::npos);
        }
        // pokud byla všechna data úspěšeně odeslána vyskočíme ze smyčky
        else {
            break;
        }
    }

    return true;

    // <PŘEVZATÝ KÓD (IPK) | KONEC>
}

/**
 * Funkce získá HTTP odpověď od serveru (v reakci na přechozí HTTP dotaz) a vrátí jí
 * @param sockfd deskriptor socketu pro dané připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param newLocation výstupní parametr, který nastavíme na novou lokaci (url) v případě,
 *                    kdy HTTP odpověď serveru nás pomocí kódu 301 na tuto lokaci přesměrovala
 * @param serverResponse výstupní přes ktrý předáme HTTP odpověď serveru
 * @return false - v případě jakékoliv chyby (a v případě přesměrování pomocí kódu 301)
 *         true  - v případě že nenastala žádná chyba
 */
bool getHTTPResponse(int* sockfd, Url url, string* newLocation, string* serverResponse) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    char msgBufReader[READ_BUFFER_SIZE];  // buffer, do kterého budeme postupně načítat odpověd serveru (s daty)
    int bytes_received;       // kolik bajtů jsme pomocí funkce read přečetli
    string messageReceived;   // zpráva, kterou jsme od serveru získali

    bool httpCodeReceived = false; // příznak že jsem od serveru obdrželi návratový kód HTTP
    bool findLocation = false;     // příznak, že nás zajímá lokace (v případě http kódu 301/302)

    // postupně získáme všechna data od serveru
    while (1) {
        bzero(msgBufReader, READ_BUFFER_SIZE); // inicializujeme buffer
        // Pokusíme se získat všechna data od serveru
        if ((bytes_received = read(*sockfd, msgBufReader, READ_BUFFER_SIZE)) < 0) {
            // chyba při čtení dat
            fprintf(stderr, "Chyba, HTTP odpověď nemohla být získána ze serveru: %s\n", (url.hostName).c_str());
            fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
            fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
            return false;
        }
        // pokud server ukončil spojení, ukončíme naopak my čtení
        else if(bytes_received == 0) {
            break;
        }

        // přidáme data načtená v této iteraci do výsledného přečtěného řetězce
        messageReceived += string(msgBufReader).substr(0, bytes_received);

   // <PŘEVZATÝ KÓD | KONEC>

        // pokud jsme zatím neobdrželi návratový kód HTTP a přečetli jsme alespoň jeden řádek HTTP odpovědi serveru
        if (httpCodeReceived == false && messageReceived.find("\r\n") != string::npos) {

            // získáme http stavový kód a podle něj určíme další akce
            HTTPCodes httpCode = getHTTPCode(messageReceived, url);
            if (httpCode == code_301 || httpCode == code_302) {
                findLocation = true;
                httpCodeReceived = true;
            }
            else if (httpCode == code_200) {
                httpCodeReceived = true;
            }
            else {
                return false;
            }
        }

        // pokud nás zajímá lokace kam máme přesměrovat požadavek kvůli obdržení kódu 301/302, tak tuto lokaci získáme
        if (findLocation == true) {
            // pokud jsme již obdrželi od serveru hlavičku, tak v ní bude nová lokace
            if (messageReceived.find("\r\n\r\n") != string::npos) {
                *newLocation = getLocation(messageReceived);
                return false; // získání dat nebylo úspěšné, musíme požadavek vznést ještě jednou, tentokrát na správné umístění
            }
        }
    }
    // vrátíme přes výstupní parametr získanou odpoveď serveru
    *serverResponse = messageReceived;

    return true;

}

/**
 * Funkce získá HTTP odpověď od serveru (v reakci na přechozí HTTP dotaz) a vrátí jí
 * @param sockfd deskriptor socketu pro dané připojení
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param newLocation výstupní parametr, který nastavíme na novou lokaci (url) v případě,
 *                    kdy HTTP odpověď serveru nás pomocí kódu 301 na tuto lokaci přesměrovala
 * @param serverResponse výstupní přes ktrý předáme HTTP odpověď serveru
 * @return false - v případě jakékoliv chyby (a v případě přesměrování pomocí kódu 301)
 *         true  - v případě že nenastala žádná chyba
 */
bool getHTTPSResponse(BIO* bio, Url url, string* newLocation, string* serverResponse) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // Kód převzat z vlastního projektu IPK
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    char msgBufReader[READ_BUFFER_SIZE];  // buffer, do kterého budeme postupně načítat odpověd serveru (s daty)
    int bytes_received;       // kolik bajtů jsme pomocí funkce read přečetli
    string messageReceived;   // zpráva, kterou jsme od serveru získali

    bool httpCodeReceived = false; // příznak že jsem od serveru obdrželi návratový kód HTTP
    bool findLocation = false;    // příznak, že nás zajímá lokace (v případě http kódu 301/302)

    // postupně získáme všechna data od serveru
    while (1) {
        bzero(msgBufReader, READ_BUFFER_SIZE); // inicializujeme buffer
        // Pokusíme se získat všechna data od serveru

        // <PŘEVZATÝ KÓD | ZAČÁTEK>
        // © Kenneth Ballard, Software Engineer, MediNotes Corp.
        // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
        // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
        // Datum publikování: 28 June 2012 (First published 22 July 2004)
        // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

        if ((bytes_received = BIO_read(bio, msgBufReader, READ_BUFFER_SIZE)) < 0) {

        // <PŘEVZATÝ KÓD (Kenneth Ballard) | KONEC>

            // chyba při čtení dat, ukončíme program
            fprintf(stderr, "Chyba, HTTPS odpověď nemohla být získána ze serveru: %s\n", (url.hostName).c_str());
            fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
            fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
            return false;
        }
        // pokud server ukončil spojení, ukončíme mi čtení
        else if(bytes_received == 0) {
            break;
        }

        // přidáme data načtená v této iteraci do výsledného přečtěného řetězce
        messageReceived += string(msgBufReader).substr(0, bytes_received);

    // <PŘEVZATÝ KÓD | KONEC>

        // pokud jsem zatím neobdrželi návratový kód HTTP a přečetli jsme alespoň jeden řádek HTTP odpovědi serveru
        if (httpCodeReceived == false && messageReceived.find("\r\n") != string::npos) {

            // získáme http stavový kód a podle něj určíme další akce
            HTTPCodes httpCode = getHTTPCode(messageReceived, url);
            if (httpCode == code_301 || httpCode == code_302) {
                findLocation = true;
                httpCodeReceived = true;
            }
            else if (httpCode == code_200) {
                httpCodeReceived = true;
            }
            else {
                return false;
            }
        }

        // pokud nás zajímá lokace kam máme přesměrovat požadavek kvůli obdržení kódu 301/302, tak tuto lokaci získáme
        if (findLocation == true) {
            // pokud jsme již obdrželi od serveru hlavičku, tak v ní bude nová lokace
            if (messageReceived.find("\r\n\r\n") != string::npos) {
                *newLocation = getLocation(messageReceived);
                return false; // získání dat nebylo úspěšné, musíme požadavek vznést ještě jednou, tentokrát na správné umístění
            }
        }
    }
    // vrátíme přes výstupní parametr získanou odpoveď serveru
    *serverResponse = messageReceived;

    return true;

}

/**
 * Funkce z HTTP hlavičky "vyzobne" stavový kód
 * @param httpResponse HTTP odpověď (musí obsahovat min. jeden řádek HTTP hlavičky)
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @return stavový kód jako hodnota z výčtu HTTPCodes
 */
HTTPCodes getHTTPCode(string httpResponse, Url url) {
    // zkontrolujeme jestli je návratový kód v pořádku
    if (httpResponse.find("HTTP/1.0 200") == 0 || httpResponse.find("HTTP/1.1 200") == 0) {
        return code_200;
    }
    else if (httpResponse.find("HTTP/1.0 301") == 0 || httpResponse.find("HTTP/1.1 301") == 0) {
        return code_301;
    }
    else if (httpResponse.find("HTTP/1.0 302") == 0 || httpResponse.find("HTTP/1.1 302") == 0) {
        return code_302;
    }
    // pokud návratový kód není v pořádku, došlo k chybě
    else {
        fprintf(stderr, "\nChyba: stavový kód v HTTP odpověďi serveru %s není možné zpracovat!\n", (url.hostName).c_str());
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());                //cout << messageReceived << endl;
        size_t tmpPosition = httpResponse.find("\r\n");
        string stateCode = httpResponse.substr(0, tmpPosition);
        fprintf(stderr, "Detail chyby: server vrátil stavový kód %s\n", stateCode.c_str());
        return code_unknown;
    }
}

/**
 * Funkce z HTTP hlavičky "vyzobne" Lokaci, kam se má klient přesměrovat
 * @param httpResponse HTTP odpověď (musí obsahovat min. hlavičku)
 * @return hodnota HTTP pole Location
 */
string getLocation(string httpResponse) {

    size_t tmpPosition = httpResponse.find("\r\nLocation:");
    if (tmpPosition == string::npos) {
        return "LOCATION_NOT_FOUND";
    }
    string tmpStr = httpResponse.substr(tmpPosition + 11, string::npos);
    tmpPosition = tmpStr.find_first_not_of(" ");
    tmpStr = tmpStr.substr(tmpPosition, string::npos);
    tmpPosition = tmpStr.find("\r\n");
    return tmpStr.substr(0, tmpPosition);

}

/**
 * Funkce provede inicializační fázi před samotným použitím knihovny OpenSSL
 */
void initOpenSSL() {
    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © Kenneth Ballard, Software Engineer, MediNotes Corp.
    // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
    // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
    // Datum publikování: 28 June 2012 (First published 22 July 2004)

    SSL_library_init();
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();

    // <PŘEVZATÝ KÓD | KONEC>
}

/**
 * Funkce načte certifikáty ze zadaných lokací (souboru, adresáře)
 * @param ctx SSL kontext
 * @param url struktura obsahující URL (včetně jeho podčástí) daného zdroje (deklarovaná v network.h)
 * @param certFileFlag příznak určující, jestli se má načítat certifikát ze souboru
 * @param url certFile soubor obsahující certifikát, použije se podle příznaku certFileFlag
 * @param certFileDir adresář, ve kterém se mají vyhledávat certifikáty, výchozí je /etc/ssl/certs
 * @return false - v případě jakékoliv chyby
 *         true  - v případě že nenastala žádná chyba
 */
bool loadCerts(SSL_CTX *ctx, Url url, bool certFileFlag, string certFile, string certFileDir) {

    // <PŘEVZATÝ KÓD | ZAČÁTEK>
    // © Kenneth Ballard, Software Engineer, MediNotes Corp.
    // Titulek: Secure programming with the OpenSSL API, Part 1: Overview of the API
    // Dostupné z: http://www.ibm.com/developerworks/library/l-openssl/
    // Datum publikování: 28 June 2012 (First published 22 July 2004)
    // Pozn.: pro potřeby tohoto projektu byl kód patřičně přizpůsoben/upraven

    // načteme certifikát
    int resultOfLoadCert;
    // pokud byl uveden název konkrtního souboru s certifikátem, použijeme tento soubor
    if (certFileFlag) {
        resultOfLoadCert = SSL_CTX_load_verify_locations(ctx, certFile.c_str(), certFileDir.c_str());
    }
    // jinak tento soubor nepoužijeme
    else {
        resultOfLoadCert = SSL_CTX_load_verify_locations(ctx, NULL, certFileDir.c_str());
    }

    // pokud se načtení certifikátu nezdařilo
    if(!resultOfLoadCert)
    {
        if (certFileFlag) {
            fprintf(stderr, "\nChyba: nepodařilo se načíst certifikát(y) ze souboru: %s nebo adresáře: %s\n", certFile.c_str(), certFileDir.c_str());
        }
        else {
            fprintf(stderr, "\nChyba: nepodařilo se načíst certifikát(y) z adresáře: %s\n", certFileDir.c_str());
        }
        fprintf(stderr, "Detail chyby: chyba nastala pro zdroj s url: %s\n", (url.url).c_str());
        fprintf(stderr, "Detail chyby: %s\n", ERR_reason_error_string(ERR_get_error()));
        return false;
    }

    // <PŘEVZATÝ KÓD | KONEC>

    return true;
}
