/*
 * Soubor:    utility.cpp
 * Vytvořeno: 12. listopad 2015, 23:55
 * Author:    Jan Herec, xherec00
 * Kódování:  UTF-8
 * Popis:     Projekt předmětu ISA, program stahuje zdroje ve formátu Atom/RSS a vypisuje z nich požadované informace na stdout
 *            Tento modul zapouzdřuje různé obecné užitečné funkce s širokým potenciálem použití
 */

#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

#include <string>

using namespace std;

/**
 * Funkce odstraní okrajové bílé znaky z daného řetězce
 * @param text řetězec, který potenciálně obsahuje okrajový bíle znaky
 * @return řetězec bez okrajových bílých znaků
 */
string trim (string text);

/**
 * Funkce zjistí, jestli je řetězec nezáporné číslo
 * @param str řetezec, který ověřujeme
 * @return false - v případě řetězec není číslo
 *         true  - v případě řetězec je číslo
 */
bool isNonNegativeNumber(string str);

#endif // UTILITY_H_INCLUDED
